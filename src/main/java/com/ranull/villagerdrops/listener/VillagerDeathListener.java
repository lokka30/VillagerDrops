package com.ranull.villagerdrops.listener;

import java.util.concurrent.ThreadLocalRandom;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

public class VillagerDeathListener implements Listener {

    @EventHandler
    public void onVillagerDeath(final EntityDeathEvent event) {
        if (event.getEntityType() != (EntityType.VILLAGER)) return;

        final Villager villager = (Villager) event.getEntity();

        for (final ItemStack itemStack : villager.getInventory()) {
            if(itemStack == null || itemStack.getType() == Material.AIR) continue;

            villager.getInventory().removeItem(itemStack);
            event.getDrops().add(itemStack);
        }

        event.getDrops().add(
            new ItemStack(
                Material.EMERALD,
                ThreadLocalRandom.current().nextInt(4)
            )
        );
    }
}
